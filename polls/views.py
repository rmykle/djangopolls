from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.http import HttpResponse

from .models import Question

# Create your views here.
def index(request):
    question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template("polls/index.html")
    context = {
        "latest_question_list": question_list
    }
    return render(request, "polls/index.html", context)

def details(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/detail.html", {"question": question})

def results(request, question_id):
    response = "Show results for id %s"
    return HttpResponse(response %question_id)

def vote(request, question_id):
    return HttpResponse("Vote for id %s" %question_id)